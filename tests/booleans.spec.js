import booleans from '../src/assets/scripts/booleans';

describe('exists(): ', () => {
  it('knows 3 is NOT in the array [1, 2, 4, 5]', () => {
    const value = booleans.exists(3, [1,2,4,5]);
    expect(value).toBe(false);
  });
  it('knows 2 is in the array [1, 2, 4, 5]', () => {
    const value = booleans.exists(2, [1, 2, 4, 5]);
    expect(value).toBeGreaterThan(-1);
  });
});

describe('isNumeric(): ', () => {
  it('Can tell 3L is not numeric', () => {
    let value = booleans.isNumeric('3l');
    expect(value).toBeFalsy();
  });

  it('Can tell 31 is numeric', () => {
    let value = booleans.isNumeric('31');
    expect(value).toBeTruthy();
  });

  it('Even negative numbers like -413 can be numeric', () => {
    let value = booleans.isNumeric('-413');
    expect(value).toBeTruthy();
  });
});

describe('isNumericDash(): ', () => {
  const x = '123-456-789-';
  it(`knows this string can be numbers and dashes only ${x}`, () => {
    let value = booleans.isNumericDash(x);
    expect(value).toBeTruthy();
  });
  const y = '123456789q';
  it(`knows this string can be numbers and dashes only ${y}`, () => {
    let value = booleans.isNumericDash(y);
    expect(value).toBeFalsy();
  });
});

describe('isAlpha(): ', () => {
  const x = 'asdf';
  it(`knows this string can be letters only ${x}`, () => {
    let value = booleans.isAlpha(x);
    expect(value).toBeTruthy();
  });
});

describe('isAlphaDash(): ', () => {
  const x = 'asdf-_-';
  it(`knows this string can be letters and dashes only ${x}`, () => {
    let value = booleans.isAlphaDash(x);
    expect(value).toBeTruthy();
  });
});

describe('isAlphaNumericDash(): ', () => {
  const x = 'asdf--111_';
  it(`knows this string can be letters, numbers, and dashes only ${x}`, () => {
    let value = booleans.isAlphaNumericDash(x);
    expect(value).toBeTruthy();
  });
});

describe('isAlphaNumeric(): ', () => {
  const x = 'asEf111';
  it(`knows this string can be letters and numbers only ${x}`, () => {
    let value = booleans.isAlphaNumeric(x);
    expect(value).toBeTruthy();
  });
});

describe('isBase64(): ', () => {
  const x = 'YXNkZg==';
  it(`knows this string is base64 ${x}`, () => {
    let value = booleans.isBase64(x);
    expect(value).toBeTruthy();
  });
});

describe('isURL(): ', () => {
  it(`http://www.dynamit.com/ is URL`, () => expect(booleans.isURL('http://www.dynamit.com/')).toBeTruthy());
  it(`https://www.dynamit.com/ is URL`, () => expect(booleans.isURL('https://www.dynamit.com/')).toBeTruthy());
  it(`//www.dynamit.com/ is URL`, () => expect(booleans.isURL('//www.dynamit.com/')).toBeTruthy());
  it(`www.dynamit.com/ is URL`, () => expect(booleans.isURL('www.dynamit.com/')).toBeTruthy());
  it(`http://dynamit.com is URL`, () => expect(booleans.isURL('http://dynamit.com')).toBeTruthy());
  it(`https://dynamit.com/ is URL`, () => expect(booleans.isURL('https://dynamit.com/')).toBeTruthy());
  it(`dynamit.com is URL`, () => expect(booleans.isURL('dynamit.com')).toBeTruthy());
  it(`ftp.dynamit.com/ is URL`, () => expect(booleans.isURL('ftp.dynamit.com/')).toBeTruthy());
  it(`zip.loop is URL`, () => expect(booleans.isURL('zip.loop')).toBeFalsy());
});

describe('isLowerCase(): ', () => {
  it(` not LowerCase`, () => expect(booleans.isLowerCase('')).toBeTruthy());
  it(`ASDF1 not LowerCase`, () => expect(booleans.isLowerCase('ASDF1')).toBeFalsy());
  it(`0123 not LowerCase`, () => expect(booleans.isLowerCase('0123')).toBeTruthy());
  it(`AAAAAaa not LowerCase`, () => expect(booleans.isLowerCase('AAAAAaa')).toBeFalsy());
  it(`AAAA not LowerCase`, () => expect(booleans.isLowerCase('AAAA')).toBeFalsy());
  it(`aaaa not LowerCase`, () => expect(booleans.isLowerCase('aaaa')).toBeTruthy());
});

describe('isUpperCase(): ', () => {
  it(` not UpperCase`, () => expect(booleans.isUpperCase('')).toBeTruthy());
  it(`ASDF1 not UpperCase`, () => expect(booleans.isUpperCase('ASDF1')).toBeTruthy());
  it(`0123 not UpperCase`, () => expect(booleans.isUpperCase('0123')).toBeTruthy());
  it(`AAAAAaa not UpperCase`, () => expect(booleans.isUpperCase('AAAAAaa')).toBeFalsy());
  it(`AAAA not UpperCase`, () => expect(booleans.isUpperCase('AAAA')).toBeTruthy());
  it(`aaaa not UpperCase`, () => expect(booleans.isUpperCase('aaaa')).toBeFalsy());
});

describe('isInt(): ', () => {
  it(` is not Int`, () => expect(booleans.isInt('')).toBeFalsy());
  it(`ASDF1 is not Int`, () => expect(booleans.isInt('ASDF1')).toBeFalsy());
  it(`0123 is not Int`, () => expect(booleans.isInt('0123')).toBeFalsy());
  it(`0 is Int`, () => expect(booleans.isInt('0')).toBeTruthy());
  it(`9.3 is not Int`, () => expect(booleans.isInt('9.3')).toBeFalsy());
  it(`-2 is not Int`, () => expect(booleans.isInt('-2')).toBeTruthy());
  it(`3. is not Int`, () => expect(booleans.isInt('3.')).toBeFalsy());
  it(`3m is not Int`, () => expect(booleans.isInt('3m')).toBeFalsy());
  it(`${null} is not Int`, () => expect(booleans.isInt(null)).toBeFalsy());
});

describe('isFloat(): ', () => {
  it(` not isFloat`, () => expect(booleans.isFloat('')).toBeFalsy());
  it(`ASDF1 not isFloat`, () => expect(booleans.isFloat('ASDF1')).toBeFalsy());
  it(`0123 not isFloat`, () => expect(booleans.isFloat('0123')).toBeFalsy());
  it(`0 not isFloat`, () => expect(booleans.isFloat('0')).toBeFalsy());
  it(`9.3 isFloat`, () => expect(booleans.isFloat('9.3')).toBeTruthy());
  it(`-2 not isFloat`, () => expect(booleans.isFloat('-2')).toBeFalsy());
  it(`3. not isFloat`, () => expect(booleans.isFloat('3.')).toBeFalsy());
  it(`3m not isFloat`, () => expect(booleans.isFloat('3m')).toBeFalsy());
  it(`${null} not isFloat`, () => expect(booleans.isFloat(null)).toBeFalsy());
});

describe('isDecimal(): ', () => {
  it(` not isDecimal`, () => expect(booleans.isDecimal('')).toBeFalsy());
  it(`ASDF1 not isDecimal`, () => expect(booleans.isDecimal('ASDF1')).toBeFalsy());
  it(`0123 not isDecimal`, () => expect(booleans.isDecimal('0123')).toBeFalsy());
  it(`0 not isDecimal`, () => expect(booleans.isDecimal('0')).toBeFalsy());
  it(`9.3 isDecimal`, () => expect(booleans.isDecimal('9.3')).toBeTruthy());
  it(`-2 not isDecimal`, () => expect(booleans.isDecimal('-2')).toBeFalsy());
  it(`3. not isDecimal`, () => expect(booleans.isDecimal('3.')).toBeFalsy());
  it(`3m not isDecimal`, () => expect(booleans.isDecimal('3m')).toBeFalsy());
  it(`${null} not isDecimal`, () => expect(booleans.isDecimal(null)).toBeFalsy());
});

describe('isHexadecimal(): ', () => {
  it(` not isHexadecimal`, () => expect(booleans.isHexadecimal('')).toBeFalsy());
  it(`#123 not isHexadecimal`, () => expect(booleans.isHexadecimal('#123')).toBeFalsy());
  it(`123 isHexadecimal`, () => expect(booleans.isHexadecimal('123')).toBeTruthy());
  it(`AEF456 isHexadecimal`, () => expect(booleans.isHexadecimal('AEF456')).toBeTruthy());
  it(`AGE987 not isHexadecimal`, () => expect(booleans.isHexadecimal('AGE987')).toBeFalsy());
});

describe('isHexColor(): ', () => {
  it(` not isHexColor`, () => expect(booleans.isHexColor('')).toBeFalsy());
  it(`#123 isHexColor`, () => expect(booleans.isHexColor('#123')).toBeTruthy());
  it(`123 isHexColor`, () => expect(booleans.isHexColor('123')).toBeTruthy());
  it(`#AEF456 isHexColor`, () => expect(booleans.isHexColor('AEF456')).toBeTruthy());
  it(`#AGE987 not isHexColor`, () => expect(booleans.isHexColor('AGE987')).toBeFalsy());
});

describe('isMD5(): ', () => {
  it(` not isMD5`, () => expect(booleans.isMD5('')).toBeFalsy());
  it(`dfaaedf45678ee1f234b7cc996e221d7 isMD5`, () => expect(booleans.isMD5('dfaaedf45678ee1f234b7cc996e221d7')).toBeTruthy());
  it(`dfaaedf45678ee1f234b7cc996e221d72 not isMD5`, () => expect(booleans.isMD5('dfaaedf45678ee1f234b7cc996e221d72')).toBeFalsy());
  it(`faaedf45678ee1f234b7cc996e1d7$d7 not isMD5`, () => expect(booleans.isMD5('faaedf45678ee1f234b7cc996e1d7$d7')).toBeFalsy());
});

describe('isVisaCreditCard(): ', () => {
  it(`4111 1111 1111 1111 isVisaCreditCard`, () => expect(booleans.isCreditCard('4111 1111 1111 1111', 'visa')).toBeTruthy());
  it(`5111 1111 1111 1111 isVisaCreditCard`, () => expect(booleans.isCreditCard('5111 1111 1111 1111', 'visa')).toBeTruthy());
  it(`4111 1111 1111 11111 isVisaCreditCard`, () => expect(booleans.isCreditCard('4111 1111 1111 11111', 'visa')).toBeFalsy());
  it(`4111-1111-1111-1111 isVisaCreditCard`, () => expect(booleans.isCreditCard('4111-1111-1111-1111', 'visa')).toBeTruthy());
  it(`4111111111111111 isVisaCreditCard`, () => expect(booleans.isCreditCard('4111111111111111', 'visa')).toBeTruthy());
});

describe('isMasterCardCreditCard(): ', () => {
  it(`5500 0000 0000 0004 isMasterCardCreditCard`, () => expect(booleans.isCreditCard('5500 0000 0000 0004', 'mastercard')).toBeTruthy());
  it(`5500 0000 0000 00041 isMasterCardCreditCard`, () => expect(booleans.isCreditCard('5500 0000 0000 00041', 'mastercard')).toBeFalsy());
  it(`4500 0000 0000 0004 isMasterCardCreditCard`, () => expect(booleans.isCreditCard('4500 0000 0000 0004', 'mastercard')).toBeFalsy());
  it(`5500-0000-0000-0004 isMasterCardCreditCard`, () => expect(booleans.isCreditCard('5500-0000-0000-0004', 'mastercard')).toBeTruthy());
  it(`5500000000000004 isMasterCardCreditCard`, () => expect(booleans.isCreditCard('5500000000000004', 'mastercard')).toBeTruthy());
});

describe('isDiscoverCreditCard(): ', () => {
  it(`6011 0000 0000 0004 isDiscoverCreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 0004', 'discover')).toBeTruthy());
  it(`6011 0000 0000 00041 isDiscoverCreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 00041', 'discover')).toBeFalsy());
  it(`5011 0000 0000 0004 isDiscoverCreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 0004', 'discover')).toBeFalsy());
  it(`6011-0000-0000-0004 isDiscoverCreditCard`, () => expect(booleans.isCreditCard('6011-0000-0000-0004', 'discover')).toBeTruthy());
  it(`6011000000000004 isDiscoverCreditCard`, () => expect(booleans.isCreditCard('6011000000000004', 'discover')).toBeTruthy());
});

describe('isAmexCreditCard(): ', () => {
  it(`3400 0000 0000 009 isAmexCreditCard`, () => expect(booleans.isCreditCard('3400 0000 0000 009', 'amex')).toBeTruthy());
  it(`3400 0000 0000 0091 isAmexCreditCard`, () => expect(booleans.isCreditCard('3400 0000 0000 0091', 'amex')).toBeFalsy());
  it(`3500 0000 0000 009 isAmexCreditCard`, () => expect(booleans.isCreditCard('3500 0000 0000 009', 'amex')).toBeFalsy());
  it(`4400 0000 0000 009 isAmexCreditCard`, () => expect(booleans.isCreditCard('4400 0000 0000 009', 'amex')).toBeFalsy());
  it(`3400-0000-0000-009 isAmexCreditCard`, () => expect(booleans.isCreditCard('3400-0000-0000-009', 'amex')).toBeTruthy());
  it(`340000000000009 isAmexCreditCard`, () => expect(booleans.isCreditCard('340000000000009', 'amex')).toBeTruthy());
});

describe('isDinersClubCreditCard(): ', () => {
  it(`3000 0000 0000 04 isDinersClubCreditCard`, () => expect(booleans.isCreditCard('3000 0000 0000 04', 'dinersClub')).toBeTruthy());
  it(`3000 0000 0000 041 isDinersClubCreditCard`, () => expect(booleans.isCreditCard('3000 0000 0000 041', 'dinersClub')).toBeFalsy());
  it(`4000 0000 0000 04 isDinersClubCreditCard`, () => expect(booleans.isCreditCard('4000 0000 0000 04', 'dinersClub')).toBeFalsy());
  it(`3000-0000-0000-04 isDinersClubCreditCard`, () => expect(booleans.isCreditCard('3000-0000-0000-04', 'dinersClub')).toBeTruthy());
  it(`30000000000004 isDinersClubCreditCard`, () => expect(booleans.isCreditCard('30000000000004', 'dinersClub')).toBeTruthy());
});

describe('isJCBCreditCard(): ', () => {
  it(`3088 0000 0000 0009 isJCBCreditCard`, () => expect(booleans.isCreditCard('3088 0000 0000 0009', 'jcb')).toBeTruthy());
  it(`4088 0000 0000 0009 isJCBCreditCard`, () => expect(booleans.isCreditCard('4088 0000 0000 0009', 'jcb')).toBeFalsy());
  it(`3088 0000 0000 00091 isJCBCreditCard`, () => expect(booleans.isCreditCard('3088 0000 0000 00091', 'jcb')).toBeFalsy());
  it(`3088-0000-0000-0009 isJCBCreditCard`, () => expect(booleans.isCreditCard('3088-0000-0000-0009', 'jcb')).toBeTruthy());
  it(`3088000000000009 isJCBCreditCard`, () => expect(booleans.isCreditCard('3088000000000009', 'jcb')).toBeTruthy());
});


describe('isVMDACreditCard(): ', () => {
  it(`4111 1111 1111 1111 isVMDACreditCard`, () => expect(booleans.isCreditCard('4111 1111 1111 1111', 'vdma')).toBeTruthy());
  it(`5111 1111 1111 1111 isVMDACreditCard`, () => expect(booleans.isCreditCard('5111 1111 1111 1111', 'vdma')).toBeTruthy());
  it(`4111 1111 1111 11111 isVMDACreditCard`, () => expect(booleans.isCreditCard('4111 1111 1111 11111', 'vdma')).toBeFalsy());
  it(`4111-1111-1111-1111 isVMDACreditCard`, () => expect(booleans.isCreditCard('4111-1111-1111-1111', 'vdma')).toBeTruthy());
  it(`4111111111111111 isVMDACreditCard`, () => expect(booleans.isCreditCard('4111111111111111', 'vdma')).toBeTruthy());
  it(`5500 0000 0000 0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('5500 0000 0000 0004', 'vdma')).toBeTruthy());
  it(`5500 0000 0000 00041 isVMDACreditCard`, () => expect(booleans.isCreditCard('5500 0000 0000 00041', 'vdma')).toBeFalsy());
  it(`4500 0000 0000 0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('4500 0000 0000 0004', 'vdma')).toBeFalsy());
  it(`5500-0000-0000-0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('5500-0000-0000-0004', 'vdma')).toBeTruthy());
  it(`5500000000000004 isVMDACreditCard`, () => expect(booleans.isCreditCard('5500000000000004', 'vdma')).toBeTruthy());
  it(`6011 0000 0000 0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 0004', 'vdma')).toBeTruthy());
  it(`6011 0000 0000 00041 isVMDACreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 00041', 'vdma')).toBeFalsy());
  it(`5011 0000 0000 0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('6011 0000 0000 0004', 'vdma')).toBeFalsy());
  it(`6011-0000-0000-0004 isVMDACreditCard`, () => expect(booleans.isCreditCard('6011-0000-0000-0004', 'vdma')).toBeTruthy());
  it(`6011000000000004 isVMDACreditCard`, () => expect(booleans.isCreditCard('6011000000000004', 'vdma')).toBeTruthy());
  it(`3400 0000 0000 009 isVMDACreditCard`, () => expect(booleans.isCreditCard('3400 0000 0000 009', 'vdma')).toBeTruthy());
  it(`3400 0000 0000 0091 isVMDACreditCard`, () => expect(booleans.isCreditCard('3400 0000 0000 0091', 'vdma')).toBeFalsy());
  it(`3500 0000 0000 009 isVMDACreditCard`, () => expect(booleans.isCreditCard('3500 0000 0000 009', 'vdma')).toBeFalsy());
  it(`4400 0000 0000 009 isVMDACreditCard`, () => expect(booleans.isCreditCard('4400 0000 0000 009', 'vdma')).toBeFalsy());
  it(`3400-0000-0000-009 isVMDACreditCard`, () => expect(booleans.isCreditCard('3400-0000-0000-009', 'vdma')).toBeTruthy());
  it(`340000000000009 isVMDACreditCard`, () => expect(booleans.isCreditCard('340000000000009', 'vdma')).toBeTruthy());
});


