const webpack = require('karma-webpack');

const conf = {
  scripts: {
    src: './src/assets/scripts/main',
    dest: 'dist/assets/scripts',
  },
  dev: true,
};
const webpackConfig = require('./webpack.config.js')(conf);

webpackConfig.module.postLoaders = [{
  test: /\.js$/,
  exclude: /(node_modules|tests)/,
  loader: 'istanbul-instrumenter',
}];

// Karma configuration
module.exports = function karmaConfig(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'tests/**/*.spec.js',
    ],

    // list of files to exclude
    exclude: [
    ],

    plugins: [
      webpack,
      'karma-jasmine',
      'karma-coverage',
      'karma-spec-reporter',
      'karma-phantomjs-launcher',
      'karma-coverage-istanbul-reporter',
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'tests/**/*spec.js': ['webpack'],
      'src/**/*.js': ['webpack'],
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // reporters: ['spec', 'coverage'],
    reporters: ['spec', 'coverage-istanbul'],

    coverageIstanbulReporter: {
      // reports can be any that are listed here: https://github.com/istanbuljs/istanbul-reports/tree/590e6b0089f67b723a1fdf57bc7ccc080ff189d7/lib
      reports: ['html', 'lcovonly', 'text-summary'],
      // base output directory. If you include %browser% in the path it will be replaced with the karma browser name
      dir: __dirname + '/reports/coverage',
      // if using webpack and pre-loaders, work around webpack breaking the source path
      fixWebpackSourcePaths: true,
      // stop istanbul outputting messages like `File [${filename}] ignored, nothing could be mapped`
      skipFilesWithNoCoverage: true,
      // Most reporters accept additional config options. You can pass these through the `report-config` option
      'report-config': {
        // all options available at: https://github.com/istanbuljs/istanbul-reports/blob/590e6b0089f67b723a1fdf57bc7ccc080ff189d7/lib/html/index.js#L135-L137
        html: {
          // outputs the report in ./coverage/html
          subdir: 'html'
        }
      },
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values:
    //    config.LOG_DISABLE ||
    //    config.LOG_ERROR ||
    //    config.LOG_WARN ||
    //    config.LOG_INFO ||
    //    config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    coverageReporter: {
      dir: 'reports/coverage',
      reporters: [
        { type: 'html', subdir: 'report-html' },
      ],
    },
    webpack: webpackConfig,
    webpackMiddleware: { noInfo: true },
  });
};
