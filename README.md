# Validinator
> An input validation library

## TODO
- Check that the binding element is a form
- Check that the binding element is a field
- Class api (e.g. .setMessage(), .registerCallback(), . on the field)
- Self Binding attributes
- Callbacks for pre and post form validate
- Callback for pre and post field validate
- Depends, a function that runs before the field is validated,
  if the function returns false the field is never validated against
  the declared rules
- Custom validation rules
- is_file_type() validator
- i18n
- ('#first').validinator.doSomething();
- ('form').validinator.isValid();
- data-validinator-priority-enabled
  - Either validate higher priority constraints first and stop on first failure (true), or validate all constraints simultaneously and show all the failing ones (false).
- whenValid({group, force})	
  - Returns a jQuery promise that will be fulfilled if and only if the Form is valid. Does not affect UI nor fires events. If group is given, it only validates fields that belong to this group. If force is given, it force validates even non-required fields (See example)
- data-validinator-group - Validate a group of elements at a time?
- reset()
- destroy()
- getErrorsMessages()
- data-validinator-multiple
  - You can add this attribute to radio / checkboxes elements like this: data-parsley-multiple="mymultiplelink" to link them together even if they don't share the same name.
- disable error messages
- disable all UI
- HTML validator support
- HTML special inputs support
- isInRange() Validator
- isInPattern() Validator
- minSelected() Validator
- maxSelected() Validator
- field = field validator
- default error focusing (first, last, none)
- data-validinator-no-focus on field
  - If this field fails, do not focus on it (if first focus strategy is on, next field would be focused, if last strategy is on, previous field would be focused)
- data-validinator-validation-threshold="10"
  - Used with trigger option above, for all key- events, do not validate until the field has a certain number of characters. Default is 3
- overall form error, message, display
- gt, gte, e, lte, lt should support comparing against another field
- telephone validator()
- color validator()
- date, datetime, time validator(), with formatting params
- birthdate, younger than 120 y.o.
- file validators
  - File Size
  - File Type
  - Image Dimensions / Ratio
- month validator
- week validator
- long / latitude validator
- identity / ssn validators
- depends on val of another field (or empty)

- disable submit button on fail setting?
- enable or disable a validator ona field
- transformer
  - Modify the field value before validating, after validating, or w/e
- sanitizers, form and field level
- scrollToTopOnError setting
- Captcha, ReCaptcha, Google Captcha generator
- input masking:
  - https://igorescobar.github.io/jQuery-Mask-Plugin/
  - https://github.com/RobinHerbots/Inputmask
- Specific CC validators: 
  - https://github.com/jessepollak/payment
  - https://github.com/jondavidjohn/payform
- Routing and Banking Validators : https://github.com/gumroad/jquery.bank


## Features
- Streaming [build system](http://gulpjs.com/)

## Quick Start
After the setup is compete, update the README with your project's info.
```
$ curl -L https://github.com/dynamit/front-end-boilerplate/archive/master.tar.gz | tar zx --strip 1
```

## Configuration
Available options, with defaults shown:

```javascript
templates: {
	src: ['src/templates/**/*', '!src/templates/+(layouts|components)/**'],
},
```
### Definitions

#### {task}.src 
 Type: `String` or `Array`  
Source files for task.

## License

The MIT License (MIT)
Copyright (c) 2016 Dynamit

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
