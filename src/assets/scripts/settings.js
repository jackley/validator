let settings = {
  form: {
    selector: 'form', // we'll default to form element if nothing is provided
    autoFocus: true, // enable or disable autofocusing on the form alltogether
    enabled: true, // should we validate the form
    trigger: null, // Should we trigger an event when validation starts

    // The type of "event" that should trigger validation to start
    // defaults to submitted, (submitted|submit|fieldChange)
    validateOn: 'submitted',
    error: {
      enabled: true, // Show an overall message that the form has failed
      message: 'Uh oh! There are problems with your form. Please Try Again',
      selector: null, // If selector is null, we'll create our own div the best we can
      klass: 'form-error', // The class of the styling for the form error container
    },
    submitter: {
      selector: '[type="submit"]',
      disableOnFail: true, // Should we apply this class, and the disabled attr?
      disabledKlass: 'disabled',  // Apply this class to the submitter
    }
  },
  fields: {
    autoFocus: true, // Are we allowed to autofocus on this field? If not move to the next element
    enabled: true, // | Function, Conditionally decide if we want to validate this field at the moment, function($field, validator)
    threshold: 3, // min length before validation
    trigger: 'event', // if specified this event will be fired upon vallidating, if the form is in submitted or fieldChange mode
    validateOn: ['focus', 'blur', 'keyup'],
    error: {
      container: {
        selector: null, // we'll use the parent if it's null
        klass: 'error-container', // if we make our own container, we'll add this klass
      },
      message: {
        group: false, // we'll only show the first one if this is true
        default: '@{v} is not valid.', // default message
        tag: 'span', // we'll wrap individual messages in this
        klass: 'error-msg', // we'll apply this klass to each message
      },
    },
  },
  validator: {
    enabled: true, // Conditionally enable and disable a specific validator
    transformer: null, // Func, transform field contents before running against this validator
    message: '@{v} is not valid.', // Validation Message
  },
};

let settingsMap = {
  'form': settings.form,
  'form.selector': settings.form.selector,
  'form.autoFocus': settings.form.autoFocus,
  'form.enabled': settings.form.enabled,
  'form.trigger': settings.form.trigger,
  'form.validateOn': settings.form.validateOn,
  'form.error': settings.form.error,
  'form.error.message': settings.form.error.message,
  'form.error.message.enabled': settings.form.error.message.enabled,
  'form.error.message.default': settings.form.error.message.default,
  'form.error.message.selector': settings.form.error.message.selector,
  'form.error.message.klass': settings.form.error.message.klass,
  'form.submitter': settings.form.submitter,
  'form.submitter.selector': settings.form.submitter.selector,
  'form.submitter.disableOnFail': settings.form.submitter.disableOnFail,
  'form.submitter.disabledKlass': settings.form.submitter.disabledKlass,
  'fields': settings.fields,
  'fields.autoFocus': settings.fields.autoFocus,
  'fields.enabled': settings.fields.enabled,
  'fields.threshold': settings.fields.threshold,
  'fields.trigger': settings.fields.trigger,
  'fields.validateOn': settings.fields.validateOn,
  'fields.error': settings.fields.error,
  'fields.error.container': settings.fields.error.container,
  'fields.error.container.selector': settings.fields.error.container.selector,
  'fields.error.container.klass': settings.fields.error.container.klass,
  'fields.error.message': settings.fields.error.message,
  'fields.error.message.group': settings.fields.error.message.group,
  'fields.error.message.default': settings.fields.error.message.default,
  'fields.error.message.tag': settings.fields.error.message.tag,
  'fields.error.message.klass': settings.fields.error.message.klass,
  'validator': settings.validator,
  'validator.enabled': settings.validator.enabled,
  'validator.transformer': settings.validator.transformer,
  'validator.message': settings.validator.message,
};

export { settings, settingsMap };
