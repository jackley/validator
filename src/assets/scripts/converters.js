import util from './utilities';
import type from './types'

const converters = {

  toDate: function(value) {
    //util.assertString(value);
    let date = Date.parse(value);
    return !isNaN(date) ? new Date(date) : null;
  },

  toFloat: function(value) {
    //util.assertString(value);
    return parseFloat(value);
  },

  toInt: function(value, radix) {
    //util.assertString(value);
    return parseInt(value, radix || 10);
  },

  toBoolean: function(value, strict = 0) {
    //util.assertString(value);
    if (strict === 1) {
      return value === '1' || value === 'true';
    }
      return value !== '0' && value !== 'false' && value !== '';
  },

  toString: function(value) {
    if (type.isNumber(value)) {
      value = value.toString(10);
    } else if (type.isUndefined(value)) {
      value = 'undefined';
    } else if (type.isNull(value)) {
      value = 'null';
    } else {
      value = value.toString();
    }
    return value;
  }
}

export default converters;
