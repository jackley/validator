import type from './types';
import validate from './validators';
import regex from './regex';
import tests from './tests/tests'

const runTests = (set) => {
  let results = [];
  if(type.isDefined(set)) {
    if (type.isArray(set)) {
      for (let i = 0; i < set.length; i++) {
        let test = tests[set[i]];
        let r = execute(test);
        console.log(r);
        results.push(r);
      }
    } else {
      let test = tests[set];
      let r = execute(test);
      console.log(r);
      results.push(r);
    }
  } else {
    let keys = Object.keys(tests);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let test = tests[key];
      let r = execute(test);
      results.push(r);
    }
  }
  return results;
}

const execute = (test) => {
  let results = [];
  results.push({value: test.name});

  for (let ii = 0; ii < test.values.length; ii++) {
    let value = test.values[ii];
    let overAllResult = false;
    let passCount = 0;

    for (let iii = 0; iii < test.patterns.length; iii++) {
      let p = test.patterns[iii];
      let result = validate.matches(value, p.pattern);

      if (result === true) {
        passCount++;
      }

      results.push(
        {
          //msg: `${test.name}: ${value} | ${p.name} | ${result}`,
          value: value,
          name: p.name,
          pattern: p.pattern,
          result: result,
        }
      );
    }
    if(test.matchType === 'OR' || test.matchType === 'ANY') {
      if (passCount > 0) {
        overAllResult = true;
      }
    } else if (test.matchType === 'AND' || test.matchType === 'ALL') {
      if (passCount === test.patterns.length ) {
        overAllResult = true;
      }
    }

    results.push({
      value: value,
      result: overAllResult,
    });
    results.push({});
  }

  return results;
}

export {tests, runTests};
