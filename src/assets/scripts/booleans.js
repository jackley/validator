import util from './utilities';
import convert from './converters';
import type from './types';
import regex from './regex';
import validators from './validators';

/**
 * This file should only contain simple boolean check functions
 * and will return only true or false ALWAYS!
 * Any type checking for data formatting should happen somewhere else!
 * Take a peek at the validators file for example
 */
const booleans = {

  exists: (v, a) => { return a.indexOf(v) !== -1 },

  isNumeric: (v) => { return validators.matches(v, regex.numeric); },

  isNumericDash: (v) => { return validators.matches(v, regex.numericDash) },

  isAlpha: (v) => { return validators.matches(v, regex.alpha) },

  isAlphaDash: (v) => { return validators.matches(v, regex.alphaDash) },

  isAlphaNumericDash: (v) => { return validators.matches(v, regex.alphaNumericDash) },

  isAlphaNumeric: (v) => { return validators.matches(v, regex.alphaNumeric); },

  isBase64: (v) => { return validators.matches(v, regex.base64); },

  isURL: (v) => { return validators.matches(v, regex.url); },

  isLowerCase: (v) => { return v === v.toLowerCase(); },

  isUpperCase: (v) => { return v === v.toUpperCase(); },

  isAscii: (v) => { return validators.matches(v, regex.ascii); },

  isInt: (v) => { return validators.matches(v, regex.int); },

  isFloat: (v) => { return validators.matches(v, regex.float); },

  isDecimal: (v) => { return validators.matches(v, regex.decimal); },

  isHexadecimal: (v) => { return validators.matches(v, regex.hexadecimal); },

  isHexColor: (v) => { return validators.matches(v, regex.hexColor); },

  isMD5: (v) => { return validators.matches(v, regex.md5); },

  isCreditCard: (v, type = 'any') => {
    let sanitized = v.replace(/[^0-9]+/g, '');
    if (!validators.matches(sanitized, regex.creditCard[type])) {
      return false;
    }
    let sum = 0;
    let digit = void 0;
    let tmpNum = void 0;
    let shouldDouble = void 0;
    for (let i = sanitized.length - 1; i >= 0; i--) {
      digit = sanitized.substring(i, i + 1);
      tmpNum = convert.toInt(digit, 10);
      if (shouldDouble) {
        tmpNum *= 2;
        if (tmpNum >= 10) {
          sum += tmpNum % 10 + 1;
        } else {
          sum += tmpNum;
        }
      } else {
        sum += tmpNum;
      }
      shouldDouble = !shouldDouble;
    }

    return !!(sum % 10 === 0 ? sanitized : false);
  },

  isDataURI: (v) => { return validators.matches(v, regex.dataURI); },

  // TODO: TYPE CHECKING! AND CONVERTING! BAD BOY!
  dateIsAfter: (valueDate, compareDate = new Date()) => {
    var original = convert.toDate(valueDate);
    var comparison = convert.toDate(compareDate);
    return !!(original && comparison && original > comparison);
  },

  // TYPE CHECKING! AND CONVERTING! BAD BOY!
  dateIsBefore: (valueDate, compareDate = new Date()) => {
    var original = convert.toDate(valueDate);
    var comparison = convert.toDate(compareDate);
    return !!(original && comparison && original < comparison);
  },

  // TODO
  dateEquals: (valueDate, compareDate = new Date()) => { },

  isEmpty: (v) => { return v.length === 0 },

  isGreaterThan: (v, c) => { return v > c; },

  isGreaterThanOrEqualTo: (v, c) => { return v >= c; },

  isEqualTo: (v, c) => { return v === c; },

  isLessThan: (v, c) => { return v < c; },

  isLessThanOrEqualTo: (v, c) => { return v <= c; },

  isDivisibleBy: (v, c) => { return v % c === 0; },

  isOdd: (v) => { return v % 2 !== 1; },

  isEven: (v) => { return v % 2 !== 0; },

  // TODO: FIGURE OUT WHAT IP REGEX WORKS
  isIP: (v) => {
    return validators.matches(v, regex.ip4) ||
      validators.matches(v, regex.ip6) ||
      validators.matches(v, regex.ip6_);
  },

  isMACAddress: (v) => {
    return validators.matches(v, regex.macAddress);
  },

  containsOneNumber: (v) => {
    return validators.matches(v, regex.one.number);
  },

  containsOneUppercaseCharacter: (v) => {
    return validators.matches(v, regex.one.uppercaseCharacter);
  },

  containsSpecialCharacter: (v) => {
    return validators.matches(v, regex.specialCharacter);
  },
}

export default booleans;

// const defaultOptions = {
  //   greaterThan: null,
  //   greaterThanOrEqualTo: null,
  //   equalTo: null,
  //   lessThan: null,
  //   lessThanOrEqualTo: null,
  //   divisbleBy: null,
  // };

  // isLength: (v, options = {}) => {

  //   const length = v.length;
  //   let valid = false;

  //   if (!type.isNull(opts.min)) {
  //     if (length >= opts.min) {
  //       valid = true;
  //     } else if (length < opts.min) {
  //       valid = false;
  //     }
  //   }

  //   if (!type.isNull(opts.max)) {
  //     if (length > opts.max) {
  //       valid = false;
  //     } else if (length <= opts.max) {
  //       valid = true;
  //     }
  //   }

  //   return valid;
  // },
