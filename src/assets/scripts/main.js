// https://sites.google.com/a/dynamit.com/wiki/dynamit/front-end-guides/dt-validator
// https://jqueryvalidation.org/documentation/
// http://parsleyjs.org/doc/index.html#ui-for-form
// http://parsleyjs.org/doc/index.html#events-list
// http://parsleyjs.org/doc/annotated-source/main.html
// http://formvalidation.io/api/#add-field
// http://formvalidation.io/validators/
// http://formvalidation.io/settings/#validator-transformer
// http://formvalidation.io/addons/recaptcha2/
// https://github.com/igorescobar/jQuery-Mask-Plugin
// https://igorescobar.github.io/jQuery-Mask-Plugin/
// https://github.com/gumroad/jquery.bank
// https://github.com/RobinHerbots/Inputmask
import { addition, subtraction } from './math';
import Validinator from './Validinator';
import validators from './validators';
//import './logger';
import {tests, runTests} from './tests';
import deepmerge from './deepmerge';

window.add = addition;
window.subtract = subtraction;

let x;

// TODO: Do we even need to bind to a form?
const form = {
  //selector: 'form', // we'll default to form element if nothing is provided
  autoFocus: true, // enable or disable autofocusing on the form alltogether
  enabled: true, // should we validate the form
  trigger:'crazyPants', // Should we trigger an event when validation starts
  // The type of "event" that should trigger validation to start
  // defaults to submitted, (submitted|submit|fieldChange)
  validateOn: 'submitted',
  debug: false, // Show console debug info
  error: {
      enabled: true, // Show an overall message that the form has failed
      message: 'Ruh Roh!',
      selector: null, // If selector is null, we'll create our own div the best we can
      //klass: 'form-errorz', // The class of the styling for the form error container

  },
  errors: {},
  submitter: {
    //selector: '[type="submit"]',
    disableOnFail: true, // Should we apply this class, and the disabled attr?
    disabledKlass: 'disabled',  // Apply this class to the submitter
  },
  fields: {
    settings: {
      autoFocus: false, // Are we allowed to autofocus on this field? If not move to the next element
      enabled: true, // | Function, Conditionally decide if we want to validate this field at the moment, function($field, validator)
      //threshold: 2, // min length before validation
      trigger: 'focus|blurz', // if specified this event will be fired upon validating, if the form is in submitted or fieldChange mode
      validateOn: ['keyup', ],
      error: {
        container: {
          // TODO: Consider insertAfter, or Append
          selector: null, // we'll use the parent if it's null
          klass: 'error-container', // if we make our own container, we'll add this klass
        },
        message: {
          group: false, // we'll only show the first one if this is true
          default: '@{v} is not valid.', // default message
          tag: 'span', // we'll wrap individual messages in this
          klass: 'error-msgz', // we'll apply this klass to each message
        },
      },
    },
    first: {
      selector: 'first',
      autoFocus: true, // Are we allowed to autofocus on this field? If not move to the next element
      enabled: true, // | Function, Conditionally decide if we want to validate this field at the moment, function($field, validator)
      //threshold: 0, // min length before validation
      trigger: 'something', // if specified this event will be fired upon vallidating, if the form is in submitted or fieldChange mode
      validateOn: 'keyup',
      threshold: 4,
      error: {
        display: true,
        container: {
          selector: null, // we'll use the parent if it's null
          klass: 'error-container', // if we make our own container, we'll add this klass
        },
        message: {
          // group: false, // we'll only show the first one if this is true
          // default: '@{v} is not valid./', // default message
          // tag: 'span', // we'll wrap individual messages in this
          // klass: 'error-msg', // we'll apply this klass to each message
        },
      },
      // el: el,
      // errors: {},
      validators: {
        settings: {
          enabled: true, // Conditionally enable and disable a specific validator
          transformer: null, // Func, transform field contents before running against this validator
          message: '@{first} is not valid.!'
        },
        isAlphaNumeric: {
          enabled: true,
          expect: true,
        },
      },
    },
    second: {
      selector: 'second',
      autoFocus: false, // Are we allowed to autofocus on this field? If not move to the next element
      enabled: true, // | Function, Conditionally decide if we want to validate this field at the moment, function($field, validator)
      //threshold: 5, // min length before validation
      trigger: 'keypress', // if specified this event will be fired upon vallidating, if the form is in submitted or fieldChange mode
      //validateOn: 'keyup',
      error: {
        container: {
          selector: null, // we'll use the parent if it's null
          klass: 'error-container', // if we make our own container, we'll add this klass
          tag: 'ul',
        },
        message: {
          group: false, // we'll only show the first one if this is true
          default: '@{v} is not valid.', // default message
          tag: 'li', // we'll wrap individual messages in this
          klass: 'error-msg', // we'll apply this klass to each message
        },
      },
      // el: el,
      // errors: {},
      validators: {
        settings: {
          enabled: true, // Conditionally enable and disable a specific validator
          transformer: null, // Func, transform field contents before running against this validator
          //message: 'Go Screw!',
        },
        // isAlphaNumeric: {
        //   enabled: true,
        //   expect: true,
        // },
        // isEqualTo: {
        //   enabled: true,
        //   expect: true,
        //   params: {
        //     compare: '1337', // if this is empty we will grab the value ourself
        //   },
        //   message: '@{v} is not equal to @{c}',
        // },
        isUppercase: {
          expect: false,
          enabled: true,
          transformer: function (v) {
            //return v.toLowerCase();
            return v;
          },
          message: '@{v} sucks',
          compare: '1337',

        }
      }
    }
  }
};

const v = new Validinator(form);
console.log(v);
console.log(v.form);

v.form.el.addEventListener('crazyPants', (e) => {
  //alert('CrazyPants Event');
});



// v.setup(form); // constructor
// v.fields(form.fields); // v.addField
// v.field('nd').selector = 'nd_field' // set opts manually
// v.form().opt = 'opt' // set opt

let testResults = runTests();
for (let i = 0; i < testResults.length; i++) {
  //console.table(testResults[i]);
}

//console.log(v);



// console.log(`isArray:`, `${v.type.isArray(x)}`);
// console.log(`isBoolean:`, `${v.type.isBoolean(x)}`);
// console.log(`isDate:`, `${v.type.isDate(x)}`);
// console.log(`isDefined:`, `${v.type.isDefined(x)}`);
// console.log(`isFloat:`, `${v.type.isFloat(x)}`);
// console.log(`isFunction:`, `${v.type.isFunction(x)}`);
// console.log(`isHash:`, `${v.type.isHash(x)}`);
// console.log(`isInteger:`, `${v.type.isInteger(x)}`);
// console.log(`isNumber:`, `${v.type.isNumber(x)}`);
// console.log(`isNull:`, `${v.type.isNull(x)}`);
// console.log(`isObject:`, `${v.type.isObject(x)}`);
// console.log(`isPromise:`, `${v.type.isPromise(x)}`);
// console.log(`isRegEx:`, `${v.type.isRegEx(x)}`);
// console.log(`isString:`, `${v.type.isString(x)}`);
// console.log(`isUndefined:`, `${v.type.isUndefined(x)}`);
