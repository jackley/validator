import merge from '../deepmerge';
import validators from '../validators';
import ValidatorSettings from '../settings/ValidatorSettings';
import type from '../types';

class Validator {

  constructor(field, config = {}, defaultSettingsOverride = false) {
    this._field = field;
    this.config = config;
    this.settings = new ValidatorSettings(config, defaultSettingsOverride);
  }

  init(validatorName) {
    this.valid = false;
    this.name = validatorName;
    this.params = this.setParams();
  }
  // getSetting(setting = 'selector') {
  //   return this.options.hasOwnProperty(setting)
  //     ? this.options[setting]
  //     : this.validatorSettings[setting];
  // }

  // hydrate() {
  //   this.valid = false;
  //   this.enabled = this.getSetting('enabled');
  //   this.tranformer = this.getSetting('transformer');
  //   this.message = this.getSetting('message');
  //   this.params = this.getSetting('params');
  // }

  setParams() {
    let params = {};
    !type.isNull(this.settings.expect) ? params.expect = this.settings.expect : '';
    !type.isNull(this.settings.enabled) ? params.enabled = this.settings.enabled : '';
    !type.isNull(this.settings.transformer) ? params.transformer = this.settings.transformer : '';
    !type.isNull(this.settings.message) ? params.message = this.settings.message : '';
    !type.isNull(this.settings.compare) ? params.compare = this.settings.compare : '';
    return params;
  }

  run(key) {
    if (!this.settings.enabled) {
      return false;
    }

    let value = this._field.el.value;
    // TODO: look into form masking
    if (!type.isNull(this.settings.transformer)) {
      if(type.isFunction(this.settings.transformer)) {
        value = this.settings.transformer(value);
      }
    }


    // validator, value, extra params, optional field object
    let result = validators.execute(
      key,
      this._field.el.value,
      this.params,
      this._field
    );

    this.valid = result.result;
    if(!this.valid) {
      this.message = result.msg;
    }

    console.log(key, this._field.el.value, result);

    return this;
  }

}

export default Validator;
