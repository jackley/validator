import merge from '../deepmerge';
import Validator from './Validator';
import util from '../utilities';
import evt from '../events';
import type from '../types';
import FieldSettings from '../settings/FieldSettings';
import ValidatorSettings from '../settings/ValidatorSettings';

class Field {

  constructor(form, config) {
    this._form = form;
    this.config = config;
    //this.settings = new FieldSettings(config, defaultSettingsOverride);
  }

  init() {
    this.el = util.getElement(this.settings.selector);
    this.validators = this.setValidators();
    this.setUpErrorContainer();
    this.bindEvents();
  }

  setUpErrorContainer() {
    let display = this.settings.get('error.display');
    let selector = this.settings.get('error.container.selector');
    let errorContainerTag = this.settings.get('error.container.tag');
    let errorContainerKlass = this.settings.get('error.container.klass');
    let errorContainerAttr = this.settings.get('error.container.attr');

    if (display) {
      if (type.isNull(selector)) {
        let errorContainer = document.createElement(errorContainerTag);
        errorContainer.classList.add(errorContainerKlass)
        this.el.parentNode.insertBefore(errorContainer, this.el.nextSibling)
        this.errorContainer = errorContainer;
      } else {
        this.errorContainer = util.getElement(selector);
      }
      this.errorContainer.setAttribute(errorContainerAttr,'');
    }
  }

  setValidators() {
    let validators = {};
    let validatorSettings = new ValidatorSettings();
    if (this.config.validators.hasOwnProperty('settings')) {
      validatorSettings.init(this.config.validators.settings);
      delete this.config.validators.settings;
    }
    Object.keys(this.config.validators).forEach((k) => {
      let v = new Validator(this, this.config.validators[k]);
      v.settings = new ValidatorSettings(validatorSettings);
      v.settings.init(this.config.validators[k]);
      v.settings.setValidator(v);
      v.init(k);
      validators[k] = v;
      console.log(v);

      // validators[k] = new Validator(this, this.config.validators[k], validatorSettings);
      // validators[k].settings.init(this.config.validators[k]);
      // validators[k].settings.setMessage(k);
      // validators[k].init();
    });
    return validators;
  }

  bindEvents() {
    if (type.isString(this.settings.validateOn)) {
      this.settings.validateOn = [this.settings.validateOn];
    }
    if(type.isArray(this.settings.validateOn)) {
      this.settings.validateOn.forEach((key) => {
        this.el.addEventListener(key, (event) => {
          this._form.process(event, this);
        });
      });
    }
  }

  // getSetting(setting = 'selector') {
  //   return this.options.hasOwnProperty(setting)
  //     ? this.options[setting]
  //     : this.fieldSettings[setting];
  // }

  // init() {
  //   this.hydrate();
  //   this.bindEvents();
  // }



}



export default Field;
