import merge from '../deepmerge';
import Field from './Field';
import util from '../utilities';
import type from '../types';
// import Submitter from './Submitter';
import FormSettings from '../settings/FormSettings';
import FieldSettings from '../settings/FieldSettings';
import evt from '../events';

import {
  settings, settingsMap
} from '../settings';

class Form {

  constructor(app, config = {}) {
    this.app = app;
    this.config = config;
    this.settings = new FormSettings(this);

  }

  init() {
    //this.hydrate();
    //this.submitter.el = util.getElement(this.submitter.selector);
    this.submitted = 0;
    this.el = util.getElement(this.settings.selector);
    this.submitter = this.settings.submitter;
    this.submitter.el = util.getElement(this.submitter.selector);
    this.fields = this.setFields();
    this.setUpErrorContainer();
    this.bindEvents();
  }

  bindEvents() {
    this.submitter.el.addEventListener('click', (event) => {
      event.preventDefault();
      this.submitted++;
      this.process(event);
    });
  }

  setUpErrorContainer() {
    let display = this.settings.get('error.display');
    let selector = this.settings.get('error.selector');
    let errorContainerTag = this.settings.get('error.tag');
    let errorContainerKlass = this.settings.get('error.klass');
    let errorContainerAttr = this.settings.get('error.attr');

    if (display) {
      if (type.isNull(selector)) {
        let errorContainer = document.createElement(errorContainerTag);
        errorContainer.classList.add(errorContainerKlass);
        this.el.insertBefore(errorContainer, this.el.childNodes[0]);
        this.errorContainer = errorContainer;
      } else {
        this.errorContainer = util.getElement(selector);
      }
      this.errorContainer.setAttribute(errorContainerAttr, '');
    }
  }

  setFields() {
    let fields = {};
    let fieldSettings = new FieldSettings();
    if(this.config.fields.hasOwnProperty('settings')) {
      fieldSettings.init(this.config.fields.settings);
      delete this.config.fields.settings;
    }
    Object.keys(this.config.fields).forEach((k) => {
      let f = new Field(this, this.config.fields[k]);
      f.settings = new FieldSettings(fieldSettings);
      f.settings.init(this.config.fields[k]);
      f.settings.setField(f);
      f.init();
      fields[k] = f;
      //console.log(f);
    });
    return fields;
  }

  process(event, field) {
    let canProcess;
    let results = [];

    if (!this.settings.enabled) {
      return false;
    }

    console.log('-----------------------------------');
    console.log(`Can we process this form?`);
    document.dispatchEvent(evt.events['form:startProcess'].event);

    this.triggerCustomEvents();


    if(field) {
      canProcess = this.canProcess(event, field);
    } else {
      canProcess = this.canProcess(event);
    }

    if(!canProcess) {
      console.log('UNABLE TO PROCESS!');
    } else {
      console.log('STARTING PROCESS!');
      // if(field) {
      //   console.log(`Processing Field`);
      //   results.push(this.startValidators(field));
      // } else {
      //   console.log(`Processing Form`);
      //   Object.keys(this.fields).forEach((k) => {
      //     results.push(this.startValidators(this.fields[k]));
      //   });
      // }
      Object.keys(this.fields).forEach((k) => {
        results.push(this.startValidators(this.fields[k]));
      });

      console.log(results);
      let errors = [];
      let nFieldPasses = 0;
      let nFieldGoal = results.length;
      this.clearFormError();
      results.forEach((field, i) => {
        let nValidatorPasses = 0;
        let nValidatorGoal = field.length;
        let f = {
          field: field[0]._field,
          el: field[0]._field.el,
          messages: [],
          validators: [],
        };

        this.clearErrors(field[0]._field);
        field.forEach((validator) => {
          if (validator.valid) {
            nValidatorPasses++;
          } else {
            f.messages.push(validator.message);
            f.validators.push({
              _: validator,
              enabled: validator.settings.enabled,
              params: validator.settings.params,
              transformer: validator.settings.transformer,
              valid: validator.valid,
            });
          }
        });
        if (nValidatorPasses === nValidatorGoal) {
          nFieldPasses++;
        } else {
          errors.push(f);
        }
      });



      if(nFieldPasses === nFieldGoal) {
        console.log('Form Passed!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        this.enableSubmitter();
      } else {
        console.log('Form Failed :-(');
        console.log(errors);
        this.displayFormError(errors);
        this.displayErrors(errors);
        this.setFocus(errors);
        this.disableSubmitter();
      }
    }
    console.log('-----------------------------------');
  }

  canProcess(event, field) {
    let validateOn = false;
    let threshold = false;
    let consoleMsg;

    if (!this.settings.enabled) {  // Form validation is turned off
      console.log(`Form Not Enabled`);
      return false;
    }

    switch (this.settings.validateOn) {
      case 'fieldChange':
        // validate anytime a field has changed, whether submitted or not
        // we need to say, is the caller of this process event, a form submission
        console.log(`Form is set to update on change, but manual submits should also trigger it.`);
        console.log(`This process event was called by ${event.type}.`);
        console.log(field);
        if(field.settings.validateOn.includes(event.type)) {
          validateOn = this.fieldMeetsThreshold(field);
        }
        break;
      case 'submit':
        // only show or update validation when we press the submit button
        // we need to say, is the caller of this process event, a form submission
        // We dont care about threshold here
        console.log(
        `Form is set to only process on click / submit.
        This process event was called by ${event.type}.`
        );
        if (event.type === 'click') {
          validateOn = true;
        } else {
          console.log(`Caller was not a submit`);
          return false;
        }
        break;
      case 'submitted':
        // Once the form has submitted we will validate the form based on the fields rules
        // we need to say if this form has/is being submitted and the fields match threshold
        // go ahead and submit the form.
        if (this.submitted > 0) {
          console.log('Unknown,', `Form is set to only process on submitted. This process event was called by ${event.type}, and this form has been submitted ${this.submitted} times.`);
          validateOn = true;
        } else {
          console.log('No,', `Form is set to only process on submitted. This process event was called by ${event.type}, and this form has been submitted ${this.submitted} times.`);
        }
        break;
    }
    return validateOn;
  }

  fieldMeetsThreshold(field) {
    if (field.settings.threshold > 0) {
      if (field.el.value.length < field.settings.threshold) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  allFieldsMeetThreshold() {
    let thresholdMet = false;
    let thresholdCounter = 0; // How many thresholds have we met

    Object.keys(this.fields).forEach((key) => {
      let f = this.fields[key];
      if(this.fieldMeetsThreshold(f)) {
        thresholdCounter++;
      }
    });

    console.log(`Threshold field expectation is ${nFields}.`);
    console.log(`Threshold Counter is ${thresholdCounter}`);

    if (thresholdCounter === nFields) {
      thresholdMet = true;
    }

    return thresholdMet;
  }

  startValidators(field) {
    console.log('Start Validators!');
    let results = [];
    Object.keys(field.validators).forEach((key) => {
      let validator = field.validators[key];
      console.log(`Running Validator ${key}`);
      if (validator.settings.enabled) {
        results.push(validator.run(key));
      }
    });
    return results;
  }

  displayErrors(errors) {
    errors.forEach((e) => {
      let display = e.field.settings.get('error.display');
      if (display) {
        let group = e.field.settings.get('error.message.group');
        let fieldMessageContainer = e.field.errorContainer;
        if (group) {
          let errorMsgEl = document.createElement(e.field.settings.get('error.message.tag'));
          errorMsgEl.classList.add(e.field.settings.get('error.message.klass'));
          errorMsgEl.innerHTML = e.field.settings.get('error.message.groupMessage');
          fieldMessageContainer.appendChild(errorMsgEl);
        } else {
          e.messages.forEach((m) => {
            let errorMsgEl = document.createElement(e.field.settings.get('error.message.tag'));
            errorMsgEl.classList.add(e.field.settings.get('error.message.klass'));
            errorMsgEl.innerHTML = m;
            fieldMessageContainer.appendChild(errorMsgEl);
          });
        }
      }
    });
  }

  clearErrors(field) {
    document.querySelectorAll(`[${field.settings.get('error.container.attr')}]`).forEach((e) => {
      e.innerHTML = '';
    });
  }

  setFocus(errors) {
    let focusSet = false;
    if (this.settings.autoFocus) {
      errors.forEach((e) => {
        if (e.field.settings.autoFocus && !focusSet) {
          e.el.focus();
          focusSet = true;
        }
      });
    }
  }

  triggerCustomEvents() {
    this.settings.trigger.forEach((s) => {
      this.el.dispatchEvent(s);
    });
  }

  displayFormError() {
    let display = this.settings.error.display;
    if (display) {
      let message = this.settings.error.message;
      let formMessageContainer = this.errorContainer;
      formMessageContainer.innerHTML = message;
    }
  }

  clearFormError() {
    document.querySelectorAll(`[${this.settings.error.attr}]`).forEach((e) => {
      e.innerHTML = '';
    });
  }

  disableSubmitter() {
    if (this.submitter.disableOnFail) {
      this.submitter.el.setAttribute('disabled',true);
      this.submitter.el.classList.add(this.settings.submitter.disabledKlass);
    }
  }

  enableSubmitter() {
    this.submitter.el.removeAttribute('disabled');
    this.submitter.el.classList.remove(this.settings.submitter.disabledKlass);
  }

}

export default Form;
