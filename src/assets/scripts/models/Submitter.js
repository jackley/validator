import merge from '../deepmerge';
import util from '../utilities';
import type from '../types';

class Submitter {

  constructor(app, o = {}) {
    this.app = app;

  }

  hydrate() {
    this.el = type.isFunction(this.config.selector) ? this.config.selector() : util.getElement(this.config.selector);
  }
}

export default Submitter;
