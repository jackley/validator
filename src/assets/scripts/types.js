import convert from './converters';

const types = {

  get: function(value) {
    return Object.prototype.toString.call(value);
  },

  isArray: function(value) {
    return types.get(value) === '[object Array]';
  },

  // Checks if the value is a boolean
  isBoolean: function(value) {
    return types.get(value) === '[object Boolean]' && typeof value === 'boolean';
  },

  // Simply checks if the object is an instance of a date
  isDate: function(value) {
    return types.get(value) === '[object Date]' && value instanceof Date;
  },

  // Returns false if the object is `null` of `undefined`
  isDefined: function(value) {
    return !types.isNull(value) && value !== undefined;
  },

  isFloat: function(value) {
    return types.isNumber(value) && !types.isInteger(value) && (value === parseFloat(value));
  },

  // Returns false if the object is not a function
  isFunction: function(value) {
    return types.get(value) === '[object Function]' && typeof value === 'function';
  },

  // Checks if the object is a hash, which is equivalent to an object that
  // is neither an array nor a function.
  isHash: function(value) {
    return types.isObject(value) && !types.isArray(value) && !types.isFunction(value);
  },

  // A simple check to verify that the value is an integer. Uses `isNumber`
  // and a simple modulo check.
  isInteger: function(value) {
    return types.isNumber(value) && value % 1 === 0;
  },

  // Checks if the value is a number. This function does not consider NaN a
  // number like many other `isNumber` functions do.
  isNumber: function(value) {
    return types.get(value) === '[object Number]' && typeof value === 'number' && !isNaN(value);
  },

  isNull: function(value) {
    return types.get(value) === '[object Null]' && value === null;
  },

  // Uses the `Object` function to check if the given argument is an object.
  isObject: function(value) {
    return value === Object(value);
  },

  // Checks if the given argument is a promise. Anything with a `then`
  // function is considered a promise.
  isPromise: function(value) {
    // return types.get(value) === '[object Promise]'
    return !!value && types.isFunction(value.then);
  },

  isRegEx: function(value) {
    return types.isObject(value) && {}.toString.call(value) === '[object RegExp]';
  },

  isString: function(value) {
    return typeof value === 'string';
  },

  // Returns false if the object is `null` of `undefined`
  isUndefined: function(value) {
    return value !== null && value === undefined && types.get(value) === '[object Undefined]';
  },

}

export default types;
