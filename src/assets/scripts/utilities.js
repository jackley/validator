import type from './types';
import convert from './converters';

const utilities = {

  isJquery: function(o) {
    return o && type.isString(o.jquery);
  },

  isDOM: function(o) {
    if (!o) {
      return false;
    }

    if (!o.querySelectorAll || !o.querySelector) {
      return false;
    }

    if (type.isObject(document) && o === document) {
      return true;
    }

    // http://stackoverflow.com/a/384380/699304
    /* istanbul ignore else */
    if (typeof HTMLElement === "object") {
      return o instanceof HTMLElement;
    } else {
      return o &&
        typeof o === "object" &&
        o !== null &&
        o.nodeType === 1 &&
        typeof o.nodeName === "string";
    }
  },

  isEmpty: function(value) {
    var attr;

    // Null and undefined are empty
    if (!type.isDefined(value)) {
      return true;
    }

    // functions are non empty
    if (type.isFunction(value)) {
      return false;
    }

    // Whitespace only strings are empty
    if (type.isString(value)) {
      return type.EMPTY_STRING_REGEXP.test(value);
    }

    // For arrays we use the length property
    if (type.isArray(value)) {
      return value.length === 0;
    }

    // Dates have no attributes but aren't empty
    if (type.isDate(value)) {
      return false;
    }

    // If we find at least one property we consider it non empty
    if (type.isObject(value)) {
      for (attr in value) {
        return false;
      }
      return true;
    }

    return false;
  },

  // Checks if the value is a number. This function does not consider NaN a
  // number like many other `isNumber` functions do.
  assertString: function(value) {
    if (!type.isString(value)) {
      throw new TypeError('This library (validator.js) validates strings only');
    }
  },

  formatMessage: function(v) {
    console.log(this);
  },

  getElement: (selector) => {
    let el;
    if (utilities.isJquery(selector)) {
      el = selector[0];
    } else {
      el = document.querySelectorAll(selector);
      if (el.length === 0) {
        el = document.querySelectorAll(`input[name="${selector}"]`);
      }
    }
    if (el.length === 1) {
      el = el[0];
    } else if (el.length > 1) {
      el = el;
    }
    return el;
  },

  luhn: (cc) => {
    var sum = 0;
    var i;

    for (i = cc.length - 2; i >= 0; i -= 2) {
      sum += Array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)[parseInt(cc.charAt(i), 10)];
    }
    for (i = cc.length - 1; i >= 0; i -= 2) {
      sum += parseInt(cc.charAt(i), 10);
    }
    return (sum % 10) == 0;
  },

  contains: (obj, value) => {
    if (!type.isDefined(obj)) {
      return false;
    }

    if (type.isArray(obj)) {
      return obj.indexOf(value) !== -1;
    }

    return value in obj;
  },

  unslug: (str) => {
    let r = str;
    r = r.replace('_', ' ').replace('-', ' ');
    return r;
  },

  slug: (str) => {
    const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
    const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return str.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(p, c =>
        b.charAt(a.indexOf(c)))     // Replace special chars
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')             // Trim - from end of text
  }



  // contains: function(obj, value) {
  //   if (!utilities.isDefined(obj)) {
  //     return false;
  //   }
  //   if (utilities.isArray(obj)) {
  //     return obj.indexOf(value) !== -1;
  //   }
  //   return value in obj;
  // },

  // unique: function(array) {
  //   if (!utilities.isArray(array)) {
  //     return array;
  //   }
  //   return array.filter(function(el, index, array) {
  //     return array.indexOf(el) == index;
  //   });
  // }
}

export default utilities;
