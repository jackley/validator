class Events {
  constructor() {
    this.events = {};
    this.registerEvents();

  }

  registerEvents() {
    this.events = {
      'form:init': {
        event: new Event('form:init'),
        message: 'form:init',
      },
      'form:startProcess': {
        event: new Event('form:startProcess'),
        message: 'form:startProcess',
      },
      'form:beforeValidate': {
        event: new Event('form:beforeValidate'),
        message: 'form:beforeValidate',
      },
      'form:error': {
        event: new Event('form:error'),
        message: 'form:error',
      },
      'form:success': {
        event: new Event('form:success'),
        message: 'form:success',
      },
      'field:init': {
        event: new Event('field:init'),
        message: 'field:init',
      },
      'field:error': {
        event: new Event('field:error'),
        message: 'field:error',
      },
      'field:success': {
        event: new Event('field:success'),
        message: 'field:success',
      },
      'field:status': {
        event: new Event('field:status'),  // Status changed
        message: 'field:status',
      },
      'field:added': {
        event: new Event('field:added'),
        message: 'field:added',
      },
      'field:removed': {
        event: new Event('field:removed'),
        message: 'field:removed',
      },
      'field:changed': {
        event: new Event('field:changed'),
        message: 'field:changed',
      },
      'validator:error': {
        event: new Event('validator:error'),
        message: 'validator:error',
      },
      'validator:success': {
        event: new Event('validator:success'),
        message: 'validator:success',
      },
    };
    // this.events = {
    //   form: {
    //     init: 'form:init', // After the script has started binding
    //     startProcess: 'form:startProcess',
    //     beforeValidate: 'form:beforeValidate', // Right before we start validating
    //     error: 'form:error',  // Form Failed
    //     success: 'form:success', // Form Succeeded
    //   },
    //   field: {
    //     init: 'field:init',
    //     error: 'field:error',
    //     success: 'field:success',
    //     status: 'field:status',  // Status changed
    //     added: 'field:added',
    //     removed: 'field:removed',
    //     changed: 'field:changed',
    //   },
    //   validator: {
    //     error: 'validator:error',
    //     success: 'validator:success',
    //   },
    // };
  }

  // addEventListener(el, event, func) {
  //   let realEvent = event.replace('_','');
  //   el.addEventListener(realEvent, func.bind(this));
  // }

  // _keypress(e) {
  //   let submitted = this.app.form.submitted;
  //   if (submitted > 0) {
  //     // process form
  //     console.log('PROCESS');
  //     console.log(e);
  //   }
  // }

  // keypress(e) {
  //   console.log('KEYPRESS');
  //   console.log(e);
  // }


}
const evt = new Events();
export default evt;
