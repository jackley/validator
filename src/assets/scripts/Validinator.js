import util from './utilities';
import type from './types';
import convert from './converters';
import validators from './validators';
import regex from './regex';
import Form from './models/form';
import evt from './events';

class Validinator {

  constructor(options) {
    this.debug = true;
    this.util = util;
    this.type = type;
    this.regex = regex;
    this.convert = convert;
    this.validators = validators;
    this.elements = {};
    this.namespace = 'vn';
    this.nodeid = 0;
    evt.app = this;
    this.form = new Form(this, options);
    this.form.settings.init(options);
    this.form.init();

    this.init();
    //this.collectElements();
   // console.log(this.form);
  }

  init() {
    this.bindEventListeners();
  }

  bindEventListeners() {
    Object.keys(evt.events).forEach((k) => {
      document.addEventListener(k, (e) => {
        if (this.debug) {
          console.log(evt.events[k].message);
        }
      });
    });
  }

  getNodeid() {
    return this.nodeid;
  }

  incrementNodeid() {
    this.nodeid++;
    return this.nodeid;
  }
}


export default Validinator;
