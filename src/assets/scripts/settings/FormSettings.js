import merge from '../deepmerge';
import type from '../types';

class FormSettings {

  constructor(form) {
    this._form = form;
    this.setDefaults();
    this.mergables = ['selector', 'autoFocus', 'enabled', 'trigger', 'validateOn', 'error', 'submitter'];
  }

  init(config) {
    this.mergeSettings(config);
    let trigger = this.get('trigger');
    if (type.isString(trigger)) {
      this.put('trigger', [new Event(trigger)]);
    } else if (type.isArray(trigger)) {
      let triggers = [];
      trigger.forEach((t) => {
        if (type.isString(t)) {
          triggers.push(new Event(trigger));
        } else {
          triggers.push(t);
        }
      });
    }
  }

  setDefaults() {
    const defaults = this.getDefaults();
    Object.keys(defaults).forEach((k, i) => {
      this[k] = defaults[k];
    });
  }

  getDefaults() {
    const defaults = {
      selector: 'form',   // we'll default to form element if nothing is provided
      autoFocus: true,     // enable or disable autofocusing on the form alltogether
      enabled: true,     // should we validate the form
      trigger: null,     // Should we trigger an event when validation starts
      // The type of "event" that should trigger validation to start
      // defaults to submitted, (submitted|submit|fieldChange)
      validateOn: 'submitted', // TODO: this brokeeee
      error: {
        display: true,   // Show an overall message that the form has failed
        message: 'Uh oh! There are problems with your form. Please Try Again',
        selector: null, // If selector is null, we'll create our own div the best we can
        klass: 'form-error', // The class of the styling for the form error container
        tag: 'div',
        attr: 'data-validinator-form-error-container',
      },
      submitter: {
        selector: '[type="submit"]',
        disableOnFail: true, // Should we apply this class, and the disabled attr?
        disabledKlass: 'disabled',  // Apply this class to the submitter
      },
    };

    return defaults;
  }

  mergeSettings(customSettings) {
    this.mergables.forEach((k, i) => {
      // console.log(customSettings, k, customSettings.hasOwnProperty(k), this.getDefaults()[k], customSettings[k]);
      if (customSettings.hasOwnProperty(k)) {
        if(type.isHash(customSettings[k])) {
          this[k] = merge(this[k], customSettings[k]);
        } else {
          this[k] = customSettings[k];
        }
      }
    });
  }

  // from Form.js -> this.settings.get('selector');
  // from outside -> Validinator.form.settings.set('formError.message', 'Test Message');
  put(key, value, obj) {
    if (typeof key === 'string') {
      return this.put(key.split('.'), value, this._form.settings);
    } else if (key.length === 1 && value !== undefined) {
      if (type.isHash(obj[key[0]])) {
        return obj[key[0]] = merge(obj[key[0]], value);
      }
      return obj[key[0]] = value;
    } else if (key.length === 0) {
      return obj;
    } else {
      return this.put(key.slice(1), value, obj[key[0]]);
    }
  }

  get(key) {
    return key.split('.').reduce((o, i) => o[i], this._form.settings);
  }
}

export default FormSettings;


// setApi() {
//   const handler = {
//     get: (target, prop) => {

//     },
//     set: (target, prop, value) => {

//     }
//   }
//   this.api = new Proxy(this._form, {});
//   this.map = {
//     'selector': this.api.selector,
//     'autoFocus': this.api.autoFocus,
//     'enabled': this.api.enabled,
//     'trigger': this.api.trigger,
//     'validateOn': this.api.validateOn,
//     'error': this.api.error,
//     'error.enabled': this.api.error.enabled,
//     'error.message': this.api.error.message,
//     'error.selector': this.api.error.selector,
//     'error.klass': this.api.error.klass,
//     'submitter': this.api.submitter,
//     'submitter.selector': this.api.submitter.selector,
//     'submitter.disableOnFail': this.api.submitter.disableOnFail,
//     'submitter.disabledKlass': this.api.submitter.disabledKlass,
//   };
// }
