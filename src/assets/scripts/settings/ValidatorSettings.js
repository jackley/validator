import merge from '../deepmerge';
import type from '../types';
import validators from '../validators';

class ValidatorSettings {

  constructor(defaultSettingsOverride = false) {
    this.setDefaults(defaultSettingsOverride);
    this.mergables = ['enabled', 'expect', 'transformer', 'message', 'compare'];

  }

  init(config) {
    this.mergeSettings(config);
  }

  setDefaults(defaultSettingsOverride) {
    const defaults = defaultSettingsOverride ? defaultSettingsOverride : this.getDefaults();
    Object.keys(defaults).forEach((k, i) => {
      this[k] = defaults[k];
    });
  }

  // setMessage(k) {
  //   this.settings.message = validators[k].msg;
  // }

  setValidator(v) {
    this._validator = v;
  }

  getDefaults() {
    const defaults = {
      expect: true,
      enabled: true, // Conditionally enable and disable a specific validator
      transformer: null, // Func, transform field contents before running against this validator
      message: null, // Validation Message @{v} is not valid.
      compare: null,
    };
    return defaults;
  }

  mergeSettings(customSettings) {
    this.mergables.forEach((k, i) => {
      if (customSettings.hasOwnProperty(k)) {
        if (type.isHash(customSettings[k])) {
          this[k] = merge(this[k], customSettings[k]);
        } else {
          this[k] = customSettings[k];
        }
      }
    });
  }
  // from Form.js -> this.settings.get('selector');
  // from outside -> Validinator.form.settings.set('formError.message', 'Test Message');
  put(key, value, obj) {
    if (typeof key === 'string') {
      return this.put(key.split('.'), value, this._validator.settings);
    } else if (key.length === 1 && value !== undefined) {
      if (type.isHash(obj[key[0]])) {
        return obj[key[0]] = merge(obj[key[0]], value);
      }
      return obj[key[0]] = value;
    } else if (key.length === 0) {
      return obj;
    } else {
      return this.put(key.slice(1), value, obj[key[0]]);
    }
  }

  get(key) {
    return key.split('.').reduce((o, i) => o[i], this._validator.settings);
  }
}

export default ValidatorSettings;
