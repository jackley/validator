import merge from '../deepmerge';
import type from '../types';

class FieldSettings {

  constructor(defaultSettingsOverride = false) {
    this.setDefaults(defaultSettingsOverride);
    this.mergables = ['selector', 'autoFocus', 'enabled', 'threshold', 'trigger', 'validateOn', 'error',];
  }

  init(config) {
    this.mergeSettings(config);
  }

  setField(field) {
    this._field = field;
  }

  setDefaults(defaultSettingsOverride) {
    const defaults = defaultSettingsOverride ? defaultSettingsOverride : this.getDefaults();
    Object.keys(defaults).forEach((k, i) => {
      this[k] = defaults[k];
    });
  }

  getDefaults() {
    const defaults = {
      selector: null, // TODO: Implement default name
      autoFocus: true, // Are we allowed to autofocus on this field? If not move to the next element
      enabled: true, // | Function, Conditionally decide if we want to validate this field at the moment, function($field, validator)
      threshold: 3, // min length before validation  TODO: BROKED
      trigger: 'event', // if specified this event will be fired upon vallidating, if the form is in submitted or fieldChange mode
      validateOn: ['focus', 'blur', 'keyup'],
      error: {
        display: true,
        container: {
          selector: null, // we'll use the parent if it's null
          klass: 'error-container', // if we make our own container, we'll add this klass
          tag: 'div',
          attr: 'data-validinator-field-error-container' // attr we set on the elements to reference them later
        },
        message: {
          group: false, // we'll only show the first one if this is true
          groupMessage: 'There are issues with this input.',
          default: '@{v} is not valid.', // default message
          tag: 'span', // we'll wrap individual messages in this
          klass: 'error-msg', // we'll apply this klass to each message
        },
      },
    };

    return defaults;
  }

  mergeSettings(customSettings) {
    this.mergables.forEach((k, i) => {
      if (customSettings.hasOwnProperty(k)) {
        if (type.isHash(customSettings[k])) {
          this[k] = merge(this[k], customSettings[k]);
        } else {
          this[k] = customSettings[k];
        }
      }
    });
  }

  // from Form.js -> this.settings.get('selector');
  // from outside -> Validinator.form.settings.set('formError.message', 'Test Message');
  put(key, value, obj) {
    if (typeof key === 'string') {
      return this.put(key.split('.'), value, this._field.settings);
    } else if (key.length === 1 && value !== undefined) {
      if (type.isHash(obj[key[0]])) {
        return obj[key[0]] = merge(obj[key[0]], value);
      }
      return obj[key[0]] = value;
    } else if (key.length === 0) {
      return obj;
    } else {
      return this.put(key.slice(1), value, obj[key[0]]);
    }
  }

  get(key) {
    return key.split('.').reduce((o, i) => o[i], this._field.settings);
  }
}

export default FieldSettings;
