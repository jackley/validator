import util from './utilities';
import convert from './converters';
import type from './types';
import regex from './regex';
import booleans from './booleans';

/**
 * This file is responsible for pretty wrapper function for the boolean results
 * The purpose here should be to format data and do any type checking and
 * conversions before we get to the booleans file. Format up bruh!
 * All methods here should accept no more than 2 parameters.
 * The first is always value, because what's the point of this file without a value.
 * The second is cfg, which can be anything, string, array, object etc.
 * if cfg is an object, there are only a few parameters that should be defined on it,
 * see the individual method for what you are allowed to define.
 */
const validators = {

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {Array} compare of strings to compare against
   */
  // TODO: Check if this works?
  exists: (v, cfg) => {
    let compare;
    // cfg is an object with a compare prop
    if(cfg.compare) {
      compare = cfg.compare;
      // cfg is a string, wtf?
      if (type.isString(compare)) {
        compare = [cfg];
      }
    }
    // cfg is an array, hopefully its what you meant for us to check against
    if(type.isArray(cfg)) {
      compare = cfg;
    }

    // cfg is a string, wtf?
    if(type.isString(cfg)) {
      compare = [cfg];
    }

    return {
      result: booleans.exists(v, compare),
      msg: "@{v} does not exist in @{list}.",
    };
  },

/**
   * @param {String} value
   * @param {Object} cfg
   */
  isNumeric: (v, cfg) => {
    return {
      result: booleans.isNumeric(v),
      msg: "@{v} is not a numeric value.",
    };
  },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isNumericDash: (v, cfg) => {
    return {
      result: booleans.isNumericDash(v),
      msg: "@{f} can only contain numbers and dashes (-). @{v} is not valid.",
    };
  },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isAlpha: (v, cfg) => {
    return {
      result: booleans.isAlpha(v),
      msg: "@{f} can only contain letters. @{v} is not valid.",
    };
   },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isAlphaDash: (v, cfg) => {
    return {
      result: booleans.isAlphaDash(value),
      msg: "@{f} can only contain letters and dashes (-). @{v} is not valid.",
    };
   },

/**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isAlphaNumeric: (v, cfg) => {
    return {
      result: booleans.isAlphaNumeric(v),
      msg: "@{f} can only container letters and numbers. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isBase64: (v, cfg) => {
    return {
      result: booleans.isBase64(v),
      msg: "@{f} must be Base64. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isURL: (v, cfg) => {
    return {
      result: booleans.isURL(v),
      msg: "@{f} can only container letters and numbers. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isLowerCase: (v, cfg) => {
    return {
      result: booleans.isLowerCase(v),
      msg: "@{f} must be lower case. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isUppercase: (v, cfg) => {
    return {
      result: booleans.isUppercase(v),
      msg: "@{f} must be upper case. @{v} is not valid.",
    };
  },

    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isAscii: (v, cfg) => {
    return {
      result: booleans.isAscii(v),
      msg: "@{f} must be ASCII. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isInt: (v, cfg) => {
    return {
      result: booleans.isInt(v),
      msg: "@{f} must be an integer. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isFloat: (v, cfg) => {
    return {
      result: booleans.isFloat(v),
      msg: "@{f} must be a float. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isDecimal: (v, cfg) => {
    return {
      result: booleans.isDecimal(v),
      msg: "@{f} must be a decimal. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isHexadecimal: (v, cfg) => {
    return {
      result: booleans.isHexadecimal(v),
      msg: "@{f} must be hexadecimal. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isHexColor: (v, cfg) => {
    return {
      result: booleans.isHexColor(v),
      msg: "@{f} must be a hexadecimal color code. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isMD5: (v, cfg) => {
    return {
      result: booleans.isMD5(v),
      msg: "@{f} must be md5. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isCreditCard: (v, cfg) => {
    return {
      result: booleans.isCreditCard(v),
      msg: "@{f} must be a credit card number. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isDataURI: (v, cfg) => {
    return {
      result: booleans.isDataURI(v),
      msg: "@{f} must be a data URI. @{v} is not valid.",
    };
  },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   *    @prop {String} compare is the date to compare against
   */
  dateIsAfter: (v, cfg) => {
    let compare;
    if(!cfg.compare) {
      cfg.compare = new Date()
    }
    return {
      result: booleans.dateIsAfter(v, compare),
      msg: "@{f} must be a date after @{c}. @{v} is not valid.",
    };
  },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   *    @prop {String} compare is the date to compare against
   */
  dateIsBefore: (v, cfg) => {
    let compare;
    if (!cfg.compare) {
      cfg.compare = new Date();
    }
    compare = cfg.compare;
    return {
      result: booleans.dateIsBefore(v, compare),
      msg: "@{f} must be a date before @{c}. @{v} is not valid.",
    };
  },

  // TODO
  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   *    @prop {String} compare is the date to compare against
   */
  dateEquals: (v, cfg) => {
    let compare;
    if (!cfg.compare) {
      cfg.compare = new Date();
    }
    compare = cfg.compare;
    return {
      result: booleans.dateEquals(v, compare),
      msg: "@{f} must be on the same day as @{c}. @{v} is not valid.",
    };
  },
  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isEmpty: (v, cfg) => {
    return {
      result: booleans.isEmpty(v),
      msg: "@{f} is empty, @{f} cannot be empty.",
    };
  },

  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   *    @prop {String} compare is the value to compare against
   */
  isGreaterThan: (v, cfg) => {
    let compare;
    if(cfg.compare) {
      compare = cfg.compare;
    }
    return {
      result: booleans.isGreaterThan(v, compare),
      msg: "@{f} must be greater than @{c}. @{v} is not valid.",
    };
  },
  /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   *    @prop {String} compare is the field to compare against
   */
  isGreaterThanOrEqualTo: (v, cfg) => {
    let compare = cfg.compare ? cfg.compare : '';
    return {
      result: booleans.isGreaterThanOrEqualTo(v, compare),
      msg: "@{f} must be greater than or equal to @{c}. @{v} is not valid.",
    };
  },

  isEqualTo: (v, cfg) => {
    let compare = cfg.compare ? cfg.compare : '';
    return {
      result: booleans.isEqualTo(v, compare),
      msg: "@{f} must be equal to @{c}. @{v} is not valid.",
    };
  },

  isLessThan: (v, cfg) => {
    let compare = cfg.compare ? cfg.compare : '';
    return {
      result: booleans.isLessThan(v, cfg),
      msg: "@{f} must be less than @{c}. @{v} is not valid.",
    };
  },

  isLessThanOrEqualTo: (v, cfg) => {
    let compare = cfg.compare ? cfg.compare : '';
    return {
      result: booleans.isLessThanOrEqualTo(v, cfg),
      msg: "@{f} must be less than or equal to @{c}. @{v} is not valid.",
    };
  },

  isDivisibleBy: (v, cfg) => {
    let compare = cfg.compare ? cfg.compare : '';
    return {
      result: booleans.isDivisibleBy(v, cfg),
      msg: "@{f} must be divisible by @{c}. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isOdd: (v, cfg) => {
    return {
      result: booleans.isOdd(v),
      msg: "@{f} must be an odd number. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isEven: (v, cfg) => {
    return {
      result: booleans.isEven(v),
      msg: "@{f} must be an even number. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isIP: (v, cfg) => {
    return {
      result: booleans.isIP(v),
      msg: "@{f} must be a valid IP Address. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  isMACAddress: (v, cfg) => {
    return {
      result: booleans.isMACAddress(v),
      msg: "@{f} must be a valid MAC Address. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  containsOneNumber: (v, cfg) => {
    return {
      result: booleans.containsOneNumber(value),
      msg: "@{f} must contain one number. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  containsOneUppercaseCharacter: (v, cfg) => {
    return {
      result: booleans.containsOneUppercaseCharacter(value),
      msg: "@{f} must contain one uppercase character. @{v} is not valid.",
    };
  },
    /**
   * @param {String} value
   * @param {Object} cfg
   *    @prop {String} field is the name of the field
   */
  containsSpecialCharacter: (v, cfg) => {
    return {
      result: booleans.containsSpecialCharacter(value),
      msg: "@{f} must contain a special character. @{v} is not valid.",
    };
  },

  contains: function (v, elem) {
    util.assertString(v);
    return v.indexOf(convert.toString(elem)) >= 0;
  },

  matches: function (str, pattern) {
    if (type.isRegEx !== '[object RegExp]') {
      pattern = new RegExp(pattern);
    }
    return pattern.test(str);
  },

  execute: (validator, value, params = {}, fieldObject = false) => {
    if (!params.enabled) {
      return false;
    }

    if (params.transformer) {
      value = params.transformer(value);
    }

    let result = validators[validator](value, params);

    if (params.expect === result.result) {
      result.result = true;
    } else {
      result.result = false;
    }

    if(result) {
      result.msg = validators.prepareMsg(result, value, params, fieldObject);
    }
    return result;
  },

  prepareMsg: (result, value, params = {}, fieldObject = false) => {
    let msg;
    if(result.msg) {
      msg = result.msg;
      if(params.message) {
        msg = params.message;
      }

      msg = msg.replace('@{v}', value);

      if(params) {
        if (params.field) {
          msg = msg.replace('@{f}', params.field);
        }
        if (params.compare) {
          msg = msg.replace('@{c}', params.compare);
        }
        if (params.list) {
          msg = msg.replace('@{list}', params.compare);
        }
      }

      if (fieldObject && !params.field) {
        msg = msg.replace('@{f}', util.unslug(util.slug(fieldObject.el.getAttribute('name'))));
      }

      if (msg.indexOf('@{f}') !== -1) {
        msg = msg.replace('@{f}', '');
      }

      if (msg.indexOf('@{c}') !== -1 || msg.indexOf('@{f}') !== -1 || msg.indexOf('@{v}') !== -1 || msg.indexOf('@{list}') !== -1) {
        msg = 'Field is not valid';
      }

      msg = msg.replace(/^-+/, '')             // Trim - from start of text
               .replace(/-+$/, '')             // Trim - from end of text

      return msg;
    }
  }


}

export default validators;

// const defaultOptions = {
  //   greaterThan: null,
  //   greaterThanOrEqualTo: null,
  //   equalTo: null,
  //   lessThan: null,
  //   lessThanOrEqualTo: null,
  //   divisbleBy: null,
  // };

  // isLength: (v, options = {}) => {

  //   const length = v.length;
  //   let valid = false;

  //   if (!type.isNull(opts.min)) {
  //     if (length >= opts.min) {
  //       valid = true;
  //     } else if (length < opts.min) {
  //       valid = false;
  //     }
  //   }

  //   if (!type.isNull(opts.max)) {
  //     if (length > opts.max) {
  //       valid = false;
  //     } else if (length <= opts.max) {
  //       valid = true;
  //     }
  //   }

  //   return valid;
  // },
