import ip from './ip';
import url from './url';

const tests = {
  ip: ip,
  url,
}

export default tests;
