import regex from '../regex';
const url = {
  name: 'URL Test',
  matchType: 'OR',
  patterns: [
    {
      name: 'url',
      pattern: regex.url,
    },
    {
      name: 'url2',
      pattern: regex.url2,
    },
    {
      name: 'url3',
      pattern: regex.url3,
    },
  ],
  values: [
    'www.google.com',
    'http://www.google.com',
    'mailto:somebody@google.com',
    'somebody@google.com',
    'www.url-with-querystring.com/?url=has-querystring',
  ],
}
export default url;
