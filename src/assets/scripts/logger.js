var baseLogFunction = console.log;
export default console.log = function() {
  baseLogFunction.apply(console, arguments);

  var args = Array.prototype.slice.call(arguments);

  let c = createDivNode();
  for(var i=0;i<args.length;i++){
    let node = createTextNode(args[i]);
    c = styleNode(c, args[i]);
    c.appendChild(node);
  }
  document.querySelector("#console-log").appendChild(c);
}

function createDivNode(message) {
  let node = document.createElement("div");
  node.style.lineHeight = '20px';
  return node;
}

function createTextNode(message){
  if(message !== 'true' && message !== 'false') {
    message = message + ' ';
  }
  let node = document.createTextNode(message);
  return node;
}

function styleNode(node, message) {
  if(message === 'true') {
    node.style.color = 'green';
  } else if (message === 'false') {
    node.style.color = '#D00';
  }
  return node;
}

window.onerror = function(message, url, linenumber) {
    console.log("JavaScript error: " + message + " on line " +
        linenumber + " for " + url);
};
