const browserSync = require('browser-sync');
const del = require('del');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const gutil = require('gulp-util');
const notifier = require('node-notifier');
const runSequence = require('run-sequence');
const webpack = require('webpack');
const reload = browserSync.reload;
const sass = require('gulp-sass');
const Server = require('karma').Server;


// configuration
const config = {
  scripts: {
    src: './src/assets/scripts/main.js',
    dest: 'dist/assets/scripts',
    watch: 'src/assets/scripts/**/*',
  },
  styles: {
    src: 'src/assets/styles/main.scss',
    dest: 'dist/assets/styles',
    watch: 'src/assets/styles/**/*',
  },
  dev: gutil.env.dev,
};


// clean
gulp.task('clean', del.bind(null, ['dist']));

// tests
gulp.task('test', (done) => {
  new Server({
    configFile: `${__dirname}/karma.conf.js`,
    singleRun: false,
  }, done).start();
});

// scripts
const webpackConfig = require('./webpack.config')(config);

gulp.task('scripts', (done) => {
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      gutil.log(gutil.colors.red(err()));
    }
    const result = stats.toJson();
    if (result.errors.length) {
      result.errors.forEach((error) => {
        gutil.log(gutil.colors.red(error));
        notifier.notify({
          title: 'JS Build Error',
          message: error,
        });
      });
    }
    done();
  });
});

gulp.task('styles', () =>
  gulp.src(config.styles.src)
    .pipe(sass({
      includePaths: './node_modules',
    }).on('error', sass.logError))
    .pipe(gulp.dest(config.styles.dest))
    .pipe(gulpif(config.dev, reload({ stream: true }))));

// server
gulp.task('serve', () => {
  browserSync({
    server: {
      baseDir: '',
    },
    notify: false,
    logPrefix: 'BrowserSync',
  });

  gulp.task('scripts:watch', ['scripts'], reload);
  gulp.watch(config.scripts.watch, ['scripts:watch']);
  gulp.task('styles:watch', ['styles']);
  gulp.watch(config.styles.watch, ['styles:watch']);
});


// default build task
gulp.task('default', ['clean'], () => {
  // define build tasks
  const tasks = [
    'scripts',
    'styles',
  ];

  // run build
  runSequence(tasks, () => {
    if (config.dev) {
      gulp.start('serve');
    }
  });
});
